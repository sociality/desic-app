const express = require('express');
const app = express();
const port = process.env.PORT || 8080;
const path = require('path');

const adminApp = path.resolve(__dirname, './dist/apps/admin-app');
const clientApp = path.resolve(__dirname, './dist/apps/client-app');

app.use('/admin', express.static(adminApp));
app.use('/app', express.static(clientApp));

app.get('/admin*', (req, res) => {
  res.sendFile('index.html', { root: adminApp }, (err) => {
    res.end();

    if (err) throw err;
  });
});

app.get('/app*', (req, res) => {
  res.sendFile('index.html', { root: clientApp }, (err) => {
    res.end();

    if (err) throw err;
  });
});

app.get('/', (req, res) => {
  res.redirect('/app');
});

app.listen(port, () =>
  console.log(`App listening at http://localhost:${port}`)
);
