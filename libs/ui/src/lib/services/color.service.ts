import { COLORS_NAME_TO_HEX } from './colorNameToHex.config';
import { Injectable } from '@angular/core';

export interface RGBObject {
  r: number;
  g: number;
  b: number;
}
export const COLOR_INTENSITY_THRESHOLD = 186;

@Injectable({
  providedIn: 'root',
})
export class ColorService {
  constructor() {}

  // https://stackoverflow.com/questions/3942878/how-to-decide-font-color-in-white-or-black-depending-on-background-color
  getContrastedTextColor(backgroundColor: string): string {
    let rgb: RGBObject;
    if (backgroundColor) {
      rgb = this.convertHexToRGB(backgroundColor);
    }
    const { r, g, b } = rgb;
    const colorIntensity = rgb.r * 0.299 + g * 0.587 + b * 0.114;
    return colorIntensity > COLOR_INTENSITY_THRESHOLD ? '#ffffff' : '#000000';
  }

  // https://stackoverflow.com/questions/1573053/javascript-function-to-convert-color-names-to-hex-codes/24390910
  convertColorNamedToHex(color: string): string {
    if (color.toLowerCase() in COLORS_NAME_TO_HEX) {
      return COLORS_NAME_TO_HEX[color.toLowerCase()];
    }
    return color;
  }

  convertHexToRGB(hex: string): RGBObject {
    let color: string;
    if (hex.indexOf('#') !== 0) {
      color = this.convertColorNamedToHex(hex);
    } else {
      color = hex;
    }
    if (hex.indexOf('#') === 0) {
      hex = hex.slice(1);
    }
    // convert 3-digit hex to 6-digits.
    if (hex.length === 3) {
      hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
    }
    if (hex.length !== 6) {
      throw new Error('Invalid HEX color.');
    }
    // invert color components
    const r = 255 - parseInt(hex.slice(0, 2), 16),
      g = 255 - parseInt(hex.slice(2, 4), 16),
      b = 255 - parseInt(hex.slice(4, 6), 16);
    // pad each with zeros and return
    return {
      r,
      g,
      b,
    };
  }
}
