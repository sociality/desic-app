import { UserInterface } from './user.interface';

export interface IncidentInterface {
  id?: string;

  title: string;
  description: string;

  incidentDate: Date;

  category: string;
  keywords: string;

  benefactor?: UserInterface;
  benefactorId?: string;
  beneficiary?: UserInterface;
  beneficiaryId: string;
}
