interface UserProfile {
  address: UserAddress;
  phone: string;
  dateOfBirth: Date;
  employmentStatus: string;
  maritalStatus: string;
}

interface UserAddress {
  street: string;
  postcode: string;
  city: string;
}

export enum UserRole {
  SUPERADMIN = 30,
  ADMIN = 20,
  USER = 10,
}

export enum UserCategory {
  LAW = 40,
  MEDICAL = 30,
  SOCIAL = 20,
  MANAGEMENT = 10,
  NONE = 0,
}

export interface AuthUserInterface {
  _id: string;
  email: string;
  access: UserRole;
  category: UserCategory;
  communicationId: string;
}

export interface UserInterface {
  id: string;
  email: string;
  name: string;
  access: UserRole;
  category?: UserCategory;
  profile?: UserProfile;
}
