export interface InternalChoiceList {
  title?: string;
  name: string;
  slug?: string;
  value: string;
}

export interface InternalContactList {
  slug: string;
  prefix?: string;
  title?: string;
  name?: string;
  icon?: string;
  value: string;
  description?: string;
}
