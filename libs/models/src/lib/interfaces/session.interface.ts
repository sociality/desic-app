import { UserInterface } from './user.interface';

export interface SessionInterface {
  id?: string;

  title: string;
  description: string;

  sessionDate: Date;

  category: string;
  keywords: string;

  benefactor?: UserInterface[];
  benefactorId?: string;
  beneficiary?: UserInterface[];
  beneficiaryId: string;
}
