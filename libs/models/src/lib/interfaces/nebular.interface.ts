export enum NEBULAR_STATUS {
  INFO = 'info', // blue
  SUCCESS = 'success', // green
  WARNING = 'warning', // orange
  DANGER = 'danger', // Pink
}
