import { APIAuthService } from './services/auth/auth.service';
import { APIConnectionsService } from './services/connections/connections.service';
import { APISubstrandsService } from './services/substrands/substrands.service';
import { APILanguagesService } from './services/languages/languages.service';
import { APIUsersService } from './services/users/users.service';
import { APIOrganizationService } from './services/organization/organization.service';

import { APIIncidentsService } from './services/incidents/incidents.service';
import { APISessionsService } from './services/sessions/sessions.service';
import { APICategoriesService } from './services/categories/categories.service';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  imports: [CommonModule, HttpClientModule],
  providers: [
    APIIncidentsService,
    APISessionsService,
    APICategoriesService,

    APIConnectionsService,
    APISubstrandsService,
    APILanguagesService,
    APIUsersService,
    APIOrganizationService,
  ],
})
export class ApiModule {}
