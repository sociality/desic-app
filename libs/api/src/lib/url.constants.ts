import { AppConfig } from './app.config';

export class URL {
  constructor() {}

  static forAuth(): string {
    return `${AppConfig.API_URL}/authentication`;
  }

  static forUsers(): string {
    return `${AppConfig.API_URL}/users`;
  }

  static forGroups(): string {
    return `${AppConfig.API_URL}/groups`;
  }

  static forCourses(): string {
    return `${AppConfig.API_URL}/groups/courses`;
  }

  static forAccess(): string {
    return `${AppConfig.API_URL}/access`;
  }

  static forStrands(): string {
    return `${AppConfig.API_URL}/strands`;
  }

  static forSubstrands(): string {
    return `${AppConfig.API_URL}/strands/substrands`;
  }

  static forLanguages(): string {
    return `${AppConfig.API_URL}/languages`;
  }

  static forPosts(): string {
    return `${AppConfig.API_URL}/posts`;
  }

  static forIncidents(): string {
    return `${AppConfig.API_URL}/incidents`;
  }

  static forSessions(): string {
    return `${AppConfig.API_URL}/sessions`;
  }

  static forExaminations(): string {
    return `${AppConfig.API_URL}/examinations`;
  }

  static forCategories(): string {
    return `${AppConfig.API_URL}/categories`;
  }

  static forConnections(): string {
    return `${AppConfig.API_URL}/connections`;
  }

  static forOrganization(): string {
    return `${AppConfig.API_URL}/organization`;
  }

  static forStatistics(): string {
    return `${AppConfig.API_URL}/statistics`;
  }

  static forCommunication(): string {
    return `${AppConfig.API_URL}/communication`;
  }
}
