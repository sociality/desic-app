import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import {
  AccessInterface,
  APIResponse,
  APIMessage,
  AccessesInterface,
} from '@desic/models';
import { URL } from './../../url.constants';

interface AuthUser {
  _id: string;
  access: string;
  email: string;
}

@Injectable({
  providedIn: 'root',
})
export class APIAccessService {
  constructor(private http: HttpClient) {}

  getAccess(userId: string): Observable<AccessInterface[]> {
    return this.http
      .get<APIResponse<AccessInterface[]>>(`${URL.forAccess()}/${userId}`)
      .pipe(map((response) => response.data.users));
  }

  updateAccess(
    userId: string,
    accesses: AccessesInterface
  ): Observable<string> {
    return this.http
      .put<APIMessage>(`${URL.forAccess()}/${userId}`, accesses)
      .pipe(map((response: APIMessage) => response.message));
  }
}
