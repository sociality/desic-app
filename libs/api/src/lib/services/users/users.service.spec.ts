import { TestBed } from '@angular/core/testing';

import { APIUsersService } from './users.service';

describe('UsersService', () => {
  let service: APIUsersService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(APIUsersService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
