import { TestBed } from '@angular/core/testing';

import { APIPostsService } from './posts.service';

describe('PostsService', () => {
  let service: APIPostsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(APIPostsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
