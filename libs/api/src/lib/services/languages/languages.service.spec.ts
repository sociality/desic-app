import { TestBed } from '@angular/core/testing';

import { APILanguagesService } from './languages.service';

describe('APILanguagesService', () => {
  let service: APILanguagesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(APILanguagesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
