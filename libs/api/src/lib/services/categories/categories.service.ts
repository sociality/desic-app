import {
  APIResponse,
  CategoryInterface,
  APIMessage,
  CategoriesInterface,
} from '@desic/models';

import { URL } from './../../url.constants';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class APICategoriesService {
  constructor(private http: HttpClient) {}

  getCategories(type: string): Observable<CategoryInterface[]> {
    return this.http
      .get<APIResponse<CategoryInterface[]>>(`${URL.forCategories()}/${type}`)
      .pipe(map((response) => response.data.categories));
  }

  updateCategories(
    categories: CategoriesInterface,
    type: string
  ): Observable<string> {
    return this.http
      .put<APIMessage>(`${URL.forCategories()}/${type}`, categories)
      .pipe(map((response: APIMessage) => response.message));
  }
}
