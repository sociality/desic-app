import { TestBed } from '@angular/core/testing';

import { APIOrganizationService } from './organization.service';

describe('OrganizationService', () => {
  let service: APIOrganizationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(APIOrganizationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
