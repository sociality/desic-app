# deSIC Network - _Web Application (Admin's & Client's Apps)_

### Description

A Data management tool for third sector organisations.

For updates in the project please visit our website [desic.io](https://www.desic.io).

This project was realised with funds from the kind people over at [NotEqual Network](https://not-equal.tech).

> Web Application is part 1 out of 2. (You can find the other part [here](https://gitlab.com/sociality/desic-api.git))

### Git

Clone [deSIC App](https://gitlab.com/sociality/desic-app.git):
`git clone https://gitlab.com/sociality/desic-app.git`
Navigate to it `cd desic-app` & run `npm install` to install dependencies.

### Run Locally

First Update **API_URL** variable into `desic-app\libs\api\src\lib\app.config.ts` file,
the URL which [deSIC API](https://gitlab.com/sociality/desic-app.git) is running on.

Run `npm run start:admin` to execute the Administrator App (starts on [localhost:4300](http://localhost:4300)) &
`npm run start:app` to execute the Client App (starts on [localhost:4200](http://localhost:4200)).

### Build App

Run `npm run build` to build the project. The build artifacts will be stored
in the `dist/apps` directory - each of the applications has a separate folder.
Use the `--prod` flag for a production build.
Access via `index.html` file - for each application.
Or run both apps using `npm run start` and access them via [localhost/app](http://localhost:8080/app/)
and [localhost/admin](http://localhost:8080/admin/)
