import { Pipe, PipeTransform } from '@angular/core';
import {
  IncidentInterface,
  SessionInterface,
  StrandInterface,
} from '@desic/models';

@Pipe({
  name: 'order_by',
  pure: false,
})
export class OrderByPipe implements PipeTransform {
  transform(
    array: IncidentInterface[] | SessionInterface[] | StrandInterface[]
  ) {
    if (array) {
      return array
        .sort
        // (
        //   a: SessionInterface | StrandInterface,
        //   b: SessionInterface | StrandInterface
        // ) => a.title.localeCompare(b.title)
        ();
    } else {
      return [];
    }
  }
}
