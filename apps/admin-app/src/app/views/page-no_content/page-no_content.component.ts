import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'admin-page-no_content',
  templateUrl: './page-no_content.component.html',
  styleUrls: ['./page-no_content.component.scss'],
})
export class PageNoContentComponent implements OnInit {
  @Input() type: string;

  constructor() {}

  ngOnInit(): void {
    console.log(this.type);
  }
}
