import {
  Component,
  OnInit,
  ViewChild,
  ChangeDetectorRef,
  OnDestroy,
  Input,
} from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormArray,
  AbstractControl,
  Validators,
} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { finalize, switchMap, takeUntil, tap } from 'rxjs/operators';

import { APIAccessService } from '@desic/api';
import { AppStateService } from '../../services/state/state.service';
import {
  AccessesInterface,
  AccessInterface,
  InternalChoiceList,
} from '@desic/models';
import { NbDialogService } from '@nebular/theme';
import { TranslateService } from '@ngx-translate/core';
import { AppChoicesService } from '../../services/choices/choices.service';

@Component({
  selector: 'admin-access-management',
  templateUrl: './access-management.component.html',
  styleUrls: ['./access-management.component.scss'],
})
export class AccessManagementComponent implements OnInit {
  @ViewChild('confirm_dialog') confirm_dialog;
  @ViewChild('response_dialog') response_dialog;
  @Input('userId') userId: string;

  users$: Observable<any[]>;
  users: any[];
  statuses: InternalChoiceList[];

  accessForm: FormGroup;
  patching = false;
  sending = false;
  private unsubscribe: Subject<any>;

  constructor(
    private fb: FormBuilder,
    private dialogService: NbDialogService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private changeDetectorRef: ChangeDetectorRef,
    private translate: TranslateService,
    private readonly choicesService: AppChoicesService,
    private readonly accessService: APIAccessService
  ) {
    this.statuses = this.choicesService.getAccessStatusList;
    this.unsubscribe = new Subject();
  }

  ngOnInit(): void {
    this.patching = false;
    this.fetchParameter();
  }

  ngOnDestroy(): void {
    this.unsubscribe.next();
    this.unsubscribe.complete();
    this.sending = false;
    this.patching = false;
  }

  private loadAccess(userId: string) {
    return this.accessService.getAccess(userId);
  }

  private fetchParameter() {
    this.loadAccess(this.userId).subscribe(
      (data) => {
        this.initForm();
        this.users = data;
        data.forEach((el) => {
          this.access.push(this.createAccessItem(el));
        });
        this.patching = true;
        this.changeDetectorRef.detectChanges();
      },
      (error) => {
        console.log(`Fetch Data Error || ${error}`);
      }
    );
  }

  initForm() {
    this.accessForm = this.fb.group({
      access: this.fb.array([], Validators.compose([])),
    });
  }

  get access() {
    return this.accessForm.get('access') as FormArray;
  }

  createAccessItem(ac: AccessInterface): FormGroup {
    return this.fb.group({
      benefactorId: [ac.id],
      accessCategory: [ac.accessCategory],
      accessAction: [ac.accessAction],
      accessStatus: [ac.accessStatus],
      accessId: [ac.accessId],
      category: [ac.category],
      name: [ac.name],
      access: [ac.accessStatus],
    });
  }

  filterStatuses(index) {
    if (this.users[index].accessStatus === 'invoke') {
      return [this.statuses[0]];
    } else {
      return [this.statuses[1], this.statuses[2]];
    }
  }

  private formatDataOnPost(controls: { [key: string]: AbstractControl }) {
    const accesses = this.accessForm.get('access').value;
    return {
      accesses: accesses,
    };
  }

  public onFormSubmit() {
    const controls = this.accessForm.controls;
    if (this.accessForm.invalid) {
      Object.keys(controls).forEach((controlName) =>
        controls[controlName].markAsTouched()
      );
      return;
    }

    this.sending = true;

    const accessData: AccessesInterface = this.formatDataOnPost(controls);
    this.openConfirmDialog(accessData);
  }

  updateAccess(accessData: AccessesInterface) {
    this.responseHandler(
      this.accessService.updateAccess(this.userId, accessData)
    );
  }

  responseHandler(message$: Observable<string>) {
    message$
      .pipe(
        tap(
          (data) => {
            this.openResponseDialog(this.response_dialog, {
              status: true,
              title: 'OK',
              message: data,
            });
          },
          (error) => {
            this.openResponseDialog(this.response_dialog, {
              status: false,
              title: 'Error',
              message: error.message,
            });
          }
        ),
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.sending = false;
          this.changeDetectorRef.markForCheck();
        })
      )
      .subscribe();
  }

  protected openConfirmDialog(accessData: AccessesInterface) {
    const dialogRef = this.dialogService.open(this.confirm_dialog);

    dialogRef.onClose.subscribe((result) => {
      if (result) {
        this.updateAccess(accessData);
      }
    });
  }

  protected openResponseDialog(_dialog, _data) {
    const dialogRef = this.dialogService.open(_dialog, { context: _data });

    dialogRef.onClose.subscribe((result) => {});
  }
}
