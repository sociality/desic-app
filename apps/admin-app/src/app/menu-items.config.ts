const LIST = {
  title: 'List',
  icon: 'list-outline',
};

const CREATE = {
  title: 'Create',
  icon: 'plus-outline',
};

export const MENU_ITEMS = [
  {
    title: 'Home',
    slug: 'home',
    icon: 'home-outline',
    link: '/',
  },
  {
    title: 'Settings',
    slug: 'settings',
    icon: 'settings-2-outline',
    children: [
      {
        title: 'Organization',
        icon: 'cube-outline',
        link: '/settings/organization',
      },
      {
        title: 'Session Categories',
        icon: 'copy-outline',
        link: '/settings/categories/edit/session',
      },
      {
        title: 'Incident Categories',
        icon: 'copy-outline',
        link: '/settings/categories/edit/incident',
      },
      {
        title: 'Examination Categories',
        icon: 'copy-outline',
        link: '/settings/categories/edit/examination',
      },
    ],
  },
  {
    title: 'Administrators',
    slug: 'administrators',
    icon: 'person-outline',
    hidden: false,
    children: [
      {
        ...LIST,
        link: '/administrators/list',
      },
      {
        title: 'Invite',
        link: '/administrators/create',
        icon: 'plus-outline',
      },
    ],
  },
  {
    title: 'Members',
    slug: 'members',
    icon: 'people-outline',
    hidden: false,
    children: [
      {
        ...LIST,
        link: '/clients/list',
      },
      {
        title: 'Create',
        link: '/clients/create',
        icon: 'plus-outline',
      },
    ],
  },
  {
    title: 'Incidents',
    slug: 'incidents',
    icon: 'calendar-outline',
    children: [
      {
        ...LIST,
        link: '/incidents/list',
      },
      {
        ...CREATE,
        title: 'Create Incident',
        link: '/incidents/create',
      },
    ],
  },
  {
    title: 'Sessions',
    slug: 'sessions',
    icon: 'calendar-outline',
    children: [
      {
        ...LIST,
        link: '/sessions/list',
      },
      {
        ...CREATE,
        title: 'Create Session',
        link: '/sessions/create',
      },
    ],
  },
  {
    title: 'Examinations',
    slug: 'examinations',
    icon: 'calendar-outline',
    children: [
      {
        ...LIST,
        link: '/examinations/list',
      },
      {
        ...CREATE,
        title: 'Create Examination',
        link: '/examinations/create',
      },
    ],
  },
  {
    title: 'Connections',
    slug: 'connections',
    icon: 'inbox-outline',
    link: '/connections/list',
    // children: [
    //   {
    //     ...LIST,
    //     link: '/connections/list',
    //   },
    //   {
    //     ...CREATE,
    //     title: 'Create Connection',
    //     link: '/connections/create',
    //   },
    // ],
  },
  {
    title: 'Posts',
    slug: 'posts',
    icon: 'clipboard-outline',
    children: [
      {
        ...LIST,
        link: '/posts/list',
      },
      {
        ...CREATE,
        link: '/posts/create',
      },
    ],
  },
];
