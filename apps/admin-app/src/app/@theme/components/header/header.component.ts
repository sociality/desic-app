import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'admin-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class AdminHeaderComponent implements OnInit {
  @Input() title: string;

  constructor(private readonly router: Router) {}

  toggleSidebar() {}

  navigateHome() {}

  logoLegend = 'Admin App';
  ngOnInit(): void {}

  goHome() {
    this.router.navigate(['/']);
  }

  logout() {
    this.router.navigate(['/auth/logout']);
  }
}
