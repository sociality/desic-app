import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import {
  NbBadgeModule,
  NbButtonModule,
  NbIconModule,
  NbLayoutModule,
  NbMenuModule,
  NbSidebarModule,
  NbThemeModule,
  NbTooltipModule,
} from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';

import { AdminHeaderComponent } from './components/header/header.component';
import { AdminMainMenuComponent } from './components/main-menu/main-menu.component';
import { AdminLayoutComponent } from './layouts/layout/layout.component';
import { AppStateService } from '../services/state/state.service';

import { ViewsModule } from '../views/views.module';
import { LocalConnectionService } from '../pages/connections/_connection.service';

@NgModule({
  declarations: [
    AdminLayoutComponent,
    AdminHeaderComponent,
    AdminMainMenuComponent,
  ],
  imports: [
    CommonModule,

    NbThemeModule.forRoot({ name: 'default' }),
    NbIconModule,
    NbLayoutModule,
    NbEvaIconsModule,
    NbSidebarModule.forRoot(),
    NbMenuModule.forRoot(),
    NbBadgeModule,
    NbTooltipModule,
    NbBadgeModule,
    NbButtonModule,

    RouterModule,
    ViewsModule,
  ],
  exports: [AdminHeaderComponent],
  providers: [AppStateService, LocalConnectionService],
})
export class ThemeModule {}
