import {
  Component,
  OnInit,
  OnDestroy,
  ChangeDetectorRef,
  ViewChild,
} from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormArray,
  Validators,
  AbstractControl,
} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { finalize, switchMap, takeUntil, tap } from 'rxjs/operators';

import { NbDialogService } from '@nebular/theme';
import { TranslateService } from '@ngx-translate/core';

import {
  APIIncidentsService,
  APICategoriesService,
  APIUsersService,
} from '@desic/api';
import {
  IncidentInterface,
  CategoryInterface,
  UserInterface,
} from '@desic/models';
import { AppStateService } from '../../../services/state/state.service';

@Component({
  selector: 'create-incident',
  templateUrl: './create-incident.component.html',
  styleUrls: ['./create-incident.component.scss'],
})
export class CreateIncidentComponent implements OnInit, OnDestroy {
  @ViewChild('confirm_dialog') confirm_dialog;
  @ViewChild('response_dialog') response_dialog;
  @ViewChild('delete_dialog') delete_dialog;

  incidentID: string = null;

  categories$: Observable<CategoryInterface[]>;
  users$: Observable<UserInterface[]>;

  incidentForm: FormGroup;
  patching: boolean = false;
  sending: boolean = false;
  private unsubscribe: Subject<any>;

  constructor(
    private fb: FormBuilder,
    private dialogService: NbDialogService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private changeDetectorRef: ChangeDetectorRef,
    private translate: TranslateService,
    private readonly appStateService: AppStateService,
    private readonly usersService: APIUsersService,
    private readonly categoriesService: APICategoriesService,
    private readonly incidentsService: APIIncidentsService
  ) {
    this.unsubscribe = new Subject();
  }

  ngOnInit(): void {
    setTimeout(() => {
      this.appStateService.setPageTitle('Incidents');
    });
    this.loadCategories();
    this.loadUsers();
    this.initForm();
    this.fetchParameter();
  }

  ngOnDestroy(): void {
    this.unsubscribe.next();
    this.unsubscribe.complete();
    this.sending = false;
    this.patching = false;
  }

  private loadIncident(id: string) {
    return this.incidentsService.getIncidentById(id);
  }

  private loadCategories() {
    this.categories$ = this.categoriesService.getCategories('incident');
  }

  private loadUsers() {
    this.users$ = this.usersService.getUsersByAccess('10');
  }

  private fetchParameter() {
    this.activatedRoute.params
      .pipe(
        switchMap(
          (params) => {
            const id = params['id'];
            if (id) {
              this.incidentID = id;

              return this.loadIncident(id).pipe(
                tap((data) => {
                  this.incidentForm.patchValue(this.formatDataOnFetch(data));
                  this.incidentForm.controls['beneficiaryId'].disable();
                  this.patching = true;
                  this.changeDetectorRef.detectChanges();
                })
              );
            } else {
              this.patching = true;
              return Promise.resolve(true);
            }
          },
          (error) => {
            console.log(`Fetch Data Error || ${error}`);
          }
        ),
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.changeDetectorRef.detectChanges();
        })
      )
      .subscribe();
  }

  private formatDataOnFetch(data: IncidentInterface) {
    return data;
  }

  private formatDataOnPost(controls: { [key: string]: AbstractControl }) {
    return {
      title: controls.title.value,
      description: controls.description.value,
      incidentDate: controls.incidentDate.value,
      category: controls.category.value,
      keywords: controls.keywords.value,
      beneficiaryId: controls.beneficiaryId.value
        ? controls.beneficiaryId.value
        : '0',
    };
  }

  initForm() {
    this.incidentForm = this.fb.group({
      beneficiaryId: [, Validators.compose([Validators.required])],

      title: ['', Validators.compose([Validators.required])],
      description: ['', Validators.compose([Validators.required])],

      incidentDate: ['', Validators.compose([Validators.required])],
      category: ['', Validators.compose([Validators.required])],
      keywords: ['', Validators.compose([Validators.required])],
    });
  }

  public onFormSubmit() {
    const controls = this.incidentForm.controls;
    if (this.incidentForm.invalid) {
      Object.keys(controls).forEach((controlName) =>
        controls[controlName].markAsTouched()
      );
      return;
    }

    this.sending = true;

    const incidentData: IncidentInterface = this.formatDataOnPost(controls);
    this.incidentID
      ? this.updateIncident(this.incidentID, incidentData)
      : this.createIncident(incidentData);
  }

  createIncident(incidentData: IncidentInterface) {
    this.responseHandler(this.incidentsService.postIncident(incidentData));
  }

  updateIncident(incidentID: string, incidentData: IncidentInterface) {
    this.responseHandler(
      this.incidentsService.updateIncident(incidentID, incidentData)
    );
  }

  responseHandler(message$: Observable<string>) {
    message$
      .pipe(
        tap(
          (data) => {
            this.openResponseDialog(this.response_dialog, {
              status: true,
              title: 'OK',
              message: data,
            });
          },
          (error) => {
            this.openResponseDialog(this.response_dialog, {
              status: false,
              title: 'Error',
              message: error.message,
            });
          }
        ),
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.sending = false;
          this.changeDetectorRef.markForCheck();
        })
      )
      .subscribe();
  }

  protected openResponseDialog(_dialog, _data) {
    const dialogRef = this.dialogService.open(_dialog, { context: _data });

    dialogRef.onClose.subscribe((result) => {
      if (_data.status) this.router.navigateByUrl('/incidents');
    });
  }

  public onClickBack(incidentId: string) {
    this.router.navigateByUrl(`/incidents/preview/${incidentId}`);
  }

  isControlHasError(controlName: string, validationType: string): boolean {
    const control = this.incidentForm.controls[controlName];
    if (!control) {
      return false;
    }

    const result =
      control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }
}
