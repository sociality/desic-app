import { waitForAsync, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminCreateCourseComponent } from './create-course.component';

describe('AdminCreateCourseComponent', () => {
  let component: AdminCreateCourseComponent;
  let fixture: ComponentFixture<AdminCreateCourseComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [AdminCreateCourseComponent],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminCreateCourseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
