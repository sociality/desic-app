import { IncidentsListComponent } from './incidents-list/incidents-list.component';
import { CreateIncidentComponent } from './create-incident/create-incident.component';
import { PreviewIncidentComponent } from './preview-incident/preview-incident.component';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

export const routes: Routes = [
  {
    path: 'list',
    component: IncidentsListComponent,
  },
  {
    path: 'create',
    component: CreateIncidentComponent,
  },
  {
    path: 'edit/:id',
    component: CreateIncidentComponent,
  },
  {
    path: 'preview/:id',
    component: PreviewIncidentComponent,
  },
  { path: '**', redirectTo: 'list' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IncidentsRoutingModule {}
