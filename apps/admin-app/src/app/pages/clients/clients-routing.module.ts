import { ClientsListComponent } from './clients-list/clients-list.component';
import { CreateClientComponent } from './create-client/create-client.component';
import { PreviewClientComponent } from './preview-client/preview-client.component';

import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

export const routes: Routes = [
  {
    path: 'list',
    component: ClientsListComponent,
  },
  {
    path: 'create',
    component: CreateClientComponent,
  },
  {
    path: 'edit/:id',
    component: CreateClientComponent,
  },
  {
    path: 'preview/:id',
    component: PreviewClientComponent,
  },
  { path: '**', redirectTo: 'list' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ClientsRoutingModule {}
