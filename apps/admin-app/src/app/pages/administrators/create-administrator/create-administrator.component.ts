import {
  Component,
  OnInit,
  OnDestroy,
  ChangeDetectorRef,
  ViewChild,
} from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormArray,
  Validators,
  AbstractControl,
} from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Observable, of, Subject } from 'rxjs';
import { finalize, mergeMap, switchMap, takeUntil, tap } from 'rxjs/operators';

import { NbDialogService } from '@nebular/theme';
import { TranslateService } from '@ngx-translate/core';

import { UserInterface, InternalChoiceList } from '@desic/models';
import { APIAuthService, APIUsersService } from '@desic/api';
import { AppStateService } from '../../../services/state/state.service';
import { AppChoicesService } from '../../../services/choices/choices.service';

@Component({
  selector: 'admin-create-administrator',
  templateUrl: './create-administrator.component.html',
  styleUrls: ['./create-administrator.component.scss'],
})
export class CreateAdministratorComponent implements OnInit, OnDestroy {
  @ViewChild('response_dialog') response_dialog;

  userID: string = null;

  categories: InternalChoiceList[] = [];

  userForm: FormGroup;
  patching: boolean = false;
  sending: boolean = false;
  private unsubscribe: Subject<any>;

  constructor(
    private fb: FormBuilder,
    private dialogService: NbDialogService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private changeDetectorRef: ChangeDetectorRef,
    private translate: TranslateService,
    private readonly appChoicesService: AppChoicesService,
    private readonly appStateService: AppStateService,
    private readonly authService: APIAuthService,
    private readonly usersService: APIUsersService
  ) {
    this.categories = this.appChoicesService.getAdministratorCategoryList;
    this.unsubscribe = new Subject();
  }

  ngOnInit(): void {
    setTimeout(() => {
      this.appStateService.setPageTitle('Administrators');
    });
    this.initForm();
    this.fetchParameter();
  }

  ngOnDestroy(): void {
    this.unsubscribe.next();
    this.unsubscribe.complete();
    this.sending = false;
    this.patching = false;
  }

  private loadUser(id: string): Observable<UserInterface> {
    return this.usersService.getUserById(id);
  }

  private fetchParameter() {
    this.activatedRoute.params
      .pipe(
        switchMap((params) => {
          const id = params['id'];

          if (id) {
            this.userID = id;

            return this.loadUser(id).pipe(
              tap((data) => {
                this.userForm.patchValue(this.formatDataOnFetch(data));
                this.userForm
                  .get('category')
                  .setValue(data.category.toString());
                this.userForm.controls['email'].disable();

                this.patching = true;
                this.changeDetectorRef.detectChanges();
              })
            );
          } else {
            this.patching = true;
            return Promise.resolve(true);
          }
        }),
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.changeDetectorRef.detectChanges();
        })
      )
      .subscribe();
  }

  initForm() {
    this.userForm = this.fb.group({
      email: [, Validators.compose([Validators.required, Validators.email])],
      name: [, Validators.compose([Validators.required])],
      category: [, Validators.compose([Validators.required])],
      phone: [, Validators.compose([Validators.required])],
      dateOfBirth: [, Validators.compose([Validators.required])],
    });
  }

  private formatDataOnFetch(data: UserInterface) {
    return {
      category: data.category.toString(),
      ...data,
      ...data.profile,
      ...data.profile.address,
      dateOfBirth: new Date(data.profile.dateOfBirth),
    };
  }

  private formatDataOnPost(controls: { [key: string]: AbstractControl }) {
    return {
      id: '',
      access: 20,
      category: parseInt(controls.category.value),
      email: controls.email.value,
      name: controls.name.value,
      profile: {
        address: {
          street: 'none',
          postcode: 'none',
          city: 'none',
        },
        phone: controls.phone.value.toString(),
        dateOfBirth: controls.dateOfBirth.value,
        maritalStatus: 'none',
        employmentStatus: 'none',
      },
    };
  }

  public onFormSubmit() {
    const controls = this.userForm.controls;
    if (this.userForm.invalid) {
      Object.keys(controls).forEach((controlName) =>
        controls[controlName].markAsTouched()
      );
      return;
    }

    this.sending = true;

    const userData: UserInterface = this.formatDataOnPost(controls);
    this.userID
      ? this.updateUser(this.userID, userData)
      : this.createUser(userData);
  }

  createUser(userData: UserInterface) {
    this.responseHandler(this.authService.createUser(userData));
  }

  updateUser(userID: string, userData: UserInterface) {
    this.responseHandler(this.usersService.updateUser(userID, userData));
  }

  responseHandler(message$: Observable<string>) {
    message$
      .pipe(
        tap(
          (data) => {
            this.openResponseDialog(this.response_dialog, {
              status: true,
              title: 'OK',
              message: data,
            });
          },
          (error) => {
            this.openResponseDialog(this.response_dialog, {
              status: false,
              title: 'Error',
              message: error.message,
            });
          }
        ),
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.sending = false;
          this.changeDetectorRef.markForCheck();
        })
      )
      .subscribe();
  }

  protected openResponseDialog(_dialog, _data) {
    const dialogRef = this.dialogService.open(_dialog, { context: _data });

    dialogRef.onClose.subscribe((result) => {
      this.router.navigateByUrl('/administrators');
    });
  }

  public onClickBack(userId: string) {
    this.router.navigateByUrl(`/administrators/preview/${userId}`);
  }

  isControlHasError(controlName: string, validationType: string): boolean {
    const control = this.userForm.controls[controlName];
    if (!control) {
      return false;
    }

    const result =
      control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }
}
