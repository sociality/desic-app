import { NbAuthModule } from '@nebular/auth';
import { AuthLoginComponent } from './auth-login/auth-login.component';
import { AuthRequestPasswordComponent } from './auth-request-password/auth-request-password.component';
import { AuthResetPasswordComponent } from './auth-reset-password/auth-reset-password.component';

import { AdminAuthRoutingModule } from './auth-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  NbCardModule,
  NbInputModule,
  NbLayoutModule,
  NbButtonModule,
  NbTabsetModule,
  NbSelectModule,
  NbRadioModule,
  NbDialogModule,
} from '@nebular/theme';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ViewsModule } from '../../views/views.module';
@NgModule({
  declarations: [
    AuthLoginComponent,
    AuthRequestPasswordComponent,
    AuthResetPasswordComponent,
  ],
  imports: [
    CommonModule,
    AdminAuthRoutingModule,
    FormsModule,
    ReactiveFormsModule,

    NbAuthModule,
    NbCardModule,
    NbInputModule,
    NbLayoutModule,
    NbButtonModule,
    NbTabsetModule,
    NbSelectModule,
    NbRadioModule,
    NbDialogModule.forRoot(),

    ViewsModule,
  ],
})
export class AdminAuthModule {}
