import { AdminPostsListComponent } from './posts-list/posts-list.component';
import { AdminCreatePostComponent } from './create-post/create-post.component';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

export const routes: Routes = [
  {
    path: 'list',
    component: AdminPostsListComponent,
  },
  {
    path: 'create',
    component: AdminCreatePostComponent,
  },
  {
    path: 'edit/:type/:id',
    component: AdminCreatePostComponent,
  },
  { path: '**', redirectTo: 'list' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminPostsRoutingModule {}
