import { AdminPostsListComponent } from './posts-list/posts-list.component';
import { AdminCreatePostComponent } from './create-post/create-post.component';
import { AdminPostCardComponent } from './post-card/post-card.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminPostsRoutingModule } from './posts-routing.module';
import {
  NbCardModule,
  NbInputModule,
  NbLayoutModule,
  NbButtonModule,
  NbTabsetModule,
  NbSelectModule,
  NbRadioModule,
  NbDatepickerModule,
  NbDialogModule,
  NbIconModule,
  NbBadgeModule,
  NbTimepickerModule,
} from '@nebular/theme';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { TranslateModule } from '@ngx-translate/core';

import { ViewsModule } from '../../views/views.module';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    AdminPostsListComponent,
    AdminCreatePostComponent,
    AdminPostCardComponent,
  ],
  imports: [
    CommonModule,
    AdminPostsRoutingModule,
    NbCardModule,
    NbInputModule,
    NbLayoutModule,
    NbButtonModule,
    NbTabsetModule,
    NbSelectModule,
    NbRadioModule,
    NbDatepickerModule.forRoot(),
    NgxMaterialTimepickerModule,
    NbDialogModule.forRoot(),
    NbIconModule,
    NbEvaIconsModule,
    NbBadgeModule,
    NbTimepickerModule.forRoot(),

    FormsModule,
    ReactiveFormsModule,
    CKEditorModule,
    TranslateModule,

    PipesModule,
    ViewsModule,
  ],
})
export class AdminPostsModule {}
