import {
  Component,
  OnInit,
  OnDestroy,
  ChangeDetectorRef,
  ViewChild,
  ElementRef,
  AfterViewInit,
  ViewChildren,
  QueryList,
} from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormArray,
  Validators,
  AbstractControl,
} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { finalize, switchMap, takeUntil, tap } from 'rxjs/operators';

import { NbDialogService, NbIconLibraries } from '@nebular/theme';
import { TranslateService } from '@ngx-translate/core';

import { APIPostsService } from '@desic/api';
import { PostInterface, POST_TYPES } from '@desic/models';
import { AppStateService } from '../../../services/state/state.service';

import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { ContentImagesComponent } from '../../../views/content-images/content-images.component';
import { formatDate } from '@angular/common';

// export class UploadAdapter {
//   private loader;
//   private editorFiles;

//   constructor(loader, editorFiles) {
//     this.loader = loader;
//     this.editorFiles = editorFiles;
//   }

//   upload() {
//     return this.loader.file.then(
//       (file) =>
//         new Promise((resolve, reject) => {
//           var myReader = new FileReader();
//           myReader.onloadend = (e) => {
//             this.editorFiles.push({
//               preview: myReader.result,
//               file: file,
//               url: null,
//             });
//             resolve({ default: myReader.result });
//           };
//           myReader.readAsDataURL(file);
//         })
//     );
//   }
// }

interface EditorFile {
  preview: string;
  file: File;
  url: string;
}
@Component({
  selector: 'admin-create-post',
  templateUrl: './create-post.component.html',
  styleUrls: ['./create-post.component.scss'],
})
export class AdminCreatePostComponent
  implements OnInit, OnDestroy, AfterViewInit {
  // @ViewChild('confirm_dialog') confirm_dialog;
  @ViewChild('response_dialog') response_dialog;
  // @ViewChild('delete_dialog') delete_dialog;
  // @ViewChild('fileInput') fileInput: ElementRef;

  @ViewChild(ContentImagesComponent, { static: false })
  public editorTextarea: ContentImagesComponent;

  /**
   * CKEditor & Variables about Content Images
   */
  public Editor = ClassicEditor;
  editorFiles: EditorFile[] = [];
  postFiles: string[] = [];

  post: PostInterface;
  postID: number;

  imageURL: string;
  previousURL: string;
  initialImage: string = '';

  postTypes = [
    { label: 'Post', value: 'post' },
    { label: 'Event', value: 'event' },
  ];

  postForm: FormGroup;
  patching = false;
  sending = false;
  private unsubscribe: Subject<any>;

  constructor(
    // private readonly coursesService: APICoursesService,
    private readonly postsService: APIPostsService,
    private fb: FormBuilder,
    private dialogService: NbDialogService,
    private iconLibraries: NbIconLibraries,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private changeDetectorRef: ChangeDetectorRef,
    private readonly appStateService: AppStateService
  ) {
    this.iconLibraries.registerFontPack('font-awesome', {
      iconClassPrefix: 'fa',
    });
    this.unsubscribe = new Subject();
  }

  // onReady(editorFiles: EditorFile[], eventData) {
  //   eventData.plugins.get('FileRepository').createUploadAdapter = function (
  //     loader
  //   ) {
  //     return new UploadAdapter(loader, editorFiles);
  //   };
  // }

  ngOnInit(): void {
    setTimeout(() => {
      this.appStateService.setPageTitle('Posts');
    });
    this.initForm();
    this.fetchParameter();
  }

  ngOnDestroy(): void {
    this.unsubscribe.next();
    this.unsubscribe.complete();
    this.sending = false;
    this.patching = false;
  }

  ngAfterViewInit(): void {
    console.log(this.editorTextarea);
  }

  private loadPost(id: number) {
    return this.postsService.getPostById(id);
  }

  private fetchParameter() {
    this.activatedRoute.params
      .pipe(
        switchMap(
          (params) => {
            const id = params['id'];
            if (id) {
              this.postID = id;

              return this.loadPost(id).pipe(
                tap((data) => {
                  this.initialImage = data.image_url;
                  this.postForm.patchValue(this.formatDataOnFetch(data));

                  this.changeDetectorRef.markForCheck();
                  this.patching = true;
                })
              );
            } else {
              this.patching = true;
              return Promise.resolve(true);
            }
          },
          (error) => {
            console.log(`Fetch Data Error || ${error}`);
          }
        ),
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.changeDetectorRef.markForCheck();
        })
      )
      .subscribe();
  }

  private formatDataOnFetch(data: PostInterface) {
    this.previousURL = data.image_url;
    this.imageURL = this.previousURL;

    this.updateValidators(false, ['image_url']);
    this.postFiles = data.contentFiles.concat([]);

    return {
      ...data,
      whenDate: data.type === POST_TYPES.EVENT ? new Date(data.when) : '',
      // whenTime:
      //   data.type === POST_TYPES.EVENT
      //     ? new Date(data.when).getHours() +
      //     ':' +
      //     new Date(data.when).getMinutes()
      //     : '',
      whenTime: data.type === POST_TYPES.EVENT ? new Date(data.when) : '',
      where: data.type === POST_TYPES.EVENT ? data.where : '',
    };
  }

  private formatDataOnPost(
    controls: { [key: string]: AbstractControl },
    editorFiles
  ) {
    const formData = new FormData();

    formData.append('title', controls.title.value);
    formData.append('description', controls.description.value);
    formData.append('image_url', controls.image_url.value);

    formData.append('type', controls.type.value);
    if (controls.type.value === POST_TYPES.EVENT) {
      formData.append(
        'when',
        this.setHoursMinutes(
          controls.whenDate.value,
          controls.whenTime.value
        ).toString()
      );
    } else {
      formData.append('when', null);
    }

    //formData.append('x', (this.setHoursMinutes(controls.whenDate.value, controls.x.value)).toString());
    formData.append('where', controls.where.value);
    formData.append('contentFiles', editorFiles.join());
    return formData;
  }

  initForm() {
    this.postForm = this.fb.group({
      title: ['', Validators.compose([Validators.required])],
      description: ['', Validators.compose([Validators.required])],

      type: ['post', Validators.compose([Validators.required])],
      whenDate: [, Validators.compose([Validators.required])],
      whenTime: [, Validators.compose([Validators.required])],
      where: [, Validators.compose([Validators.required])],
      image_url: [null, Validators.compose([Validators.required])],
      contentFiles: [null],
    });
  }

  updateValidators(required: boolean, controls: string[]) {
    if (required) {
      controls.forEach((el) => {
        this.postForm.controls[el].setValidators([Validators.required]);
        this.postForm.controls[el].updateValueAndValidity();
      });
    } else {
      controls.forEach((el) => {
        this.postForm.controls[el].clearValidators();
        this.postForm.controls[el].updateValueAndValidity();
      });
    }
  }

  setHoursMinutes(date: Date, time: Date) {
    const d = new Date(date);
    d.setHours(time.getHours());
    d.setMinutes(time.getMinutes());

    return d;
  }

  setHoursMinutes2(date: Date, time: String) {
    //NGX Time Picket
    const hoursAndMinutes = time.split(':');
    const d = new Date(date);
    d.setHours(parseInt(hoursAndMinutes[0], 10));
    return new Date(d.setMinutes(parseInt(hoursAndMinutes[1], 10)));
  }

  onFinalStep(editorFiles: string[]) {
    const controls = this.postForm.controls;

    if (this.postForm.invalid) {
      Object.keys(controls).forEach((controlName) =>
        controls[controlName].markAsTouched()
      );
      return;
    }
    this.sending = true;

    const postData: FormData = this.formatDataOnPost(controls, editorFiles);

    this.postID
      ? this.updatePost(this.postID, postData)
      : this.createPost(postData);
  }

  public async onFormSubmit() {
    const controls = this.postForm.controls;
    /** check form */
    this.updateValidators(
      controls.type.value === POST_TYPES.EVENT ? true : false,
      ['whenDate', 'whenTime', 'where']
    );

    if (this.postForm.invalid) {
      Object.keys(controls).forEach((controlName) =>
        controls[controlName].markAsTouched()
      );
      return;
    }

    await this.editorTextarea.uploadContentFiles();
  }

  createPost(postData: FormData) {
    this.responseHandler(this.postsService.postPost(postData));
  }

  updatePost(postID: number, postData: FormData) {
    this.responseHandler(this.postsService.updatePost(postID, postData));
  }

  responseHandler(message$: Observable<string>) {
    message$
      .pipe(
        tap(
          (data) => {
            this.openResponseDialog(this.response_dialog, {
              status: true,
              title: 'OK',
              message: data,
            });
          },
          (error) => {
            this.openResponseDialog(this.response_dialog, {
              status: false,
              title: 'Error',
              message: error.message,
            });
          }
        ),
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.sending = false;
          this.changeDetectorRef.markForCheck();
        })
      )
      .subscribe();
  }

  // protected openConfirmDialog(postData) {
  //   const dialogRef = this.dialogService.open(this.confirm_dialog, {
  //     context: { slug: this.post ? this.post.slug : postData.title[0].content },
  //   });

  //   dialogRef.onClose.subscribe((result) => {
  //     if (result) {
  //       postData.slug = result;
  //       if (this.post) {
  //         const controls = this.postForm.controls;
  //         controls.type.value === POST_TYPES.EDUCATIONAL
  //           ? this.updateEducationalPost(this.post.id, postData)
  //           : this.updateGeneralPost(this.post.id, postData);
  //       } else {
  //         const controls = this.postForm.controls;
  //         controls.type.value === POST_TYPES.EDUCATIONAL
  //           ? this.createEducationalPost(postData)
  //           : this.createGeneralPost(postData);
  //       }
  //     }
  //   });
  // }

  protected openResponseDialog(_dialog, _data) {
    const dialogRef = this.dialogService.open(_dialog, { context: _data });
    dialogRef.onClose.subscribe((result) => {
      if (_data.status) this.router.navigateByUrl('/posts');
    });
  }

  isControlHasError(controlName: string, validationType: string): boolean {
    const control = this.postForm.controls[controlName];
    if (!control) {
      return false;
    }

    const result =
      control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }
}
