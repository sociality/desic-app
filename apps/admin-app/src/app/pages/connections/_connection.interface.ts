import { ConnectionInterface } from '@desic/models';

interface DesicConnection {
  id: string;
  secret: string;
  sender: {
    id: string;
    email: string;
    name: string;
    access: number;
  };
  senderId: string;
  receiver: {
    id: string;
    email: string;
    name: string;
    access: number;
  };
  receiverId: string;
}

interface PeerConnection {}

interface PeerMessage {
  text: string;
  date: Date;
  reply: boolean;
  type: string;
  files: any[];
  user: {
    name: string;
  };
}

interface PeerSession {
  peer: string;
  connectionId: string;
  secret: string;
  // conn: any;
  messages: PeerMessage[];
  unread: number;
}

export interface LocalConnectionInterface {
  DesicConnection: DesicConnection;
  DesicConnections: DesicConnection[];

  PeerConnection: PeerConnection;
  PeerMessage: PeerMessage;
  PeerSession: PeerSession;
  PeerSessions: PeerSession[];
}
