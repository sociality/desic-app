import { Observable, Subject, Subscription } from 'rxjs';
import { APIAuthService, APIConnectionsService } from '@desic/api';
import {
  AuthUserInterface,
  ConnectionInterface,
  SessionInterface,
} from '@desic/models';

import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { AppStateService } from '../../../services/state/state.service';
import Peer from 'peerjs';
import * as CryptoJS from 'crypto-js';

/** Local Service & Interface */
import { LocalConnectionService } from '../_connection.service';
import { LocalConnectionInterface } from '../_connection.interface';

@Component({
  selector: 'admin-connections-list',
  templateUrl: './connections-list.component.html',
  styleUrls: ['./connections-list.component.scss'],
})
export class ConnectionsListComponent implements OnInit {
  public mypeerid: string = '';
  public sessions: LocalConnectionInterface['PeerSessions'] = [];

  public incomingPeerConnections: any[] = [];
  public outgoingPeerConnections: any[] = [];
  desicConnections$: Observable<ConnectionInterface[]>;
  desicConnections: LocalConnectionInterface['DesicConnections'];
  desicConnection: LocalConnectionInterface['DesicConnection'];
  peer: any;

  currentConnectionId = '';

  private unsubscribe: Subject<any>;
  private subscription: Subscription = new Subscription();

  constructor(
    private changeDetectorRef: ChangeDetectorRef,
    private readonly connectionService: APIConnectionsService,
    private readonly appStateService: AppStateService,
    private readonly authService: APIAuthService,
    private localConnectionService: LocalConnectionService
  ) {
    this.mypeerid = (this.authService.currentUser as AuthUserInterface)._id;

    this.subscription.add(
      this.localConnectionService.peer.subscribe((data) => (this.peer = data))
    );
    this.subscription.add(
      this.localConnectionService.peerSessions.subscribe(
        (data) => (this.sessions = data)
      )
    );
    this.subscription.add(
      this.localConnectionService.desicConnections.subscribe(
        (data) => (this.desicConnections = data)
      )
    );
    this.subscription.add(
      this.localConnectionService.desicConnection.subscribe(
        (data) => (this.desicConnection = data)
      )
    );
    this.subscription.add(
      this.localConnectionService.anything.subscribe(
        (data) => (this.desicConnections$ = data)
      )
    );
  }

  ngOnInit(): void {
    setTimeout(() => {
      this.appStateService.setPageTitle('Connection - MyID: ' + this.mypeerid);
    });
  }

  ngOnDestroy() {
    this.localConnectionService.changeDesicConnection(null);
    this.subscription.unsubscribe();
  }

  private loadConnections() {
    this.desicConnections$ = this.connectionService.getConnectionsAdmin();
    this.localConnectionService.changeAnything(this.desicConnections$);
    this.desicConnections$.subscribe((data) => {
      this.desicConnections = data;
      this.localConnectionService.changeDesicConnections(this.desicConnections);
    });
  }

  public openMessages(connection: ConnectionInterface) {
    if (parseInt(connection.id) == 0) {
      this.connectionService
        .postConnection(connection.receiver['communicationId'])
        .subscribe((data) => {
          this.loadConnections();
          this.localConnectionService.changeDesicConnection(data);
        });
    } else {
      this.localConnectionService.changeDesicConnection(connection);
    }
    const index = this.sessions
      .map((o) => {
        return o.connectionId;
      })
      .indexOf(connection.id);

    if (index > -1) {
      this.sessions[index].unread = 0;
    }

    this.localConnectionService.changePeerSessions(this.sessions);
  }

  getUnreadMessages(connectionID: string) {
    const index = this.sessions
      .map((o) => {
        return o.connectionId;
      })
      .indexOf(connectionID);

    if (index < 0) {
      return '';
    } else {
      const unread = this.sessions[index].unread;
      return unread > 0 ? unread : '';
    }
  }

  getColor(connectionID: string) {
    if (
      parseInt(connectionID) != 0 &&
      this.desicConnection.id == connectionID
    ) {
      return '#cccccc';
    }
  }

  returnPeer() {
    return this.peer;
  }

  // private loadConnections() {
  //   this.desicConnections$ = this.connectionService.getConnections();
  //   // this.localConnectionService.changeAnything(this.desicConnections$);
  //   this.desicConnections$.subscribe((data) => {
  //     this.desicConnections = data;
  //     console.log(data);
  //     this.localConnectionService.changeDesicConnections(this.desicConnections);
  //   });
  // }

  // private initializePeer() {
  //   this.peer = new Peer(this.mypeerid);

  //   this.peer.on('connection', (conn: any) => {
  //     conn.on('data', (data: any) => {
  //       this.storeMessages(conn, data);
  //     });
  //     conn.on('error', (error: any) => {
  //       console.log(`Error (Incoming Data): ${error}`);
  //     });
  //   });

  //   this.peer.on('error', (error: any) => {
  //     console.log(`Error (on Peer Connection): ${error}`);
  //   });
  // }

  // getTotalUnread() {
  //   return  this.sessions.reduce((accum,item) => accum + item.unread, 0)
  // }

  // storeMessages(conn, message) {
  //   const secret: string = this.desicConnections.filter((o) => {
  //     return o.receiver.id == conn.peer;
  //   })[0].secret;
  //   const connectionId: string = this.desicConnections.filter((o) => {
  //     return o.receiver.id == conn.peer;
  //   })[0].id;

  //   if (this.sessions && !this.sessions[0].connectionId) {
  //     this.sessions[0] = {
  //       peer: conn.peer,
  //       secret: secret,
  //       connectionId: connectionId,
  //       messages: [
  //         { sender: 'other', text: this.decryptData(message, secret) },
  //       ],
  //       unread:
  //         connectionId == this.desicConnection.id
  //           ? 0
  //           : this.sessions[0].unread + 1,
  //     };
  //   }
  //   else {
  //   const index = this.sessions
  //     .map((o) => {
  //       return o.peer;
  //     })
  //     .indexOf(conn.peer);
  //   console.log('I get an index: ', index);

  //   if (index < 0) {
  //     this.sessions.push({
  //       peer: conn.peer,
  //       secret: secret,
  //       connectionId: connectionId,
  //       messages: [
  //         { sender: 'other', text: this.decryptData(message, secret) },
  //       ],
  //       unread:
  //         connectionId == this.desicConnection.id
  //           ? 0
  //           : this.sessions[index].unread,
  //     });
  //   } else {
  //     this.sessions[index].messages.push({
  //       sender: 'other',
  //       text: this.decryptData(message, secret),
  //     });
  //     if (connectionId == this.desicConnection.id) {
  //       this.sessions[index].unread = 0;
  //     } else {
  //       this.sessions[index].unread = this.sessions[index].unread + 1;
  //     }
  //   }
  //   }
  //   this.localConnectionService.changePeerSessions(this.sessions);

  //   console.log('My messages');
  //   console.log(this.sessions);
  // }

  /** Encrypt & Decrypt Messages */
  // encryptData(data: string, secret: string) {
  //   try {
  //     return CryptoJS.AES.encrypt(JSON.stringify(data), secret).toString();
  //   } catch (e) {
  //     console.log(e);
  //   }
  // }

  // decryptData(data: any, secret: string) {
  //   try {
  //     const bytes = CryptoJS.AES.decrypt(data, secret);
  //     if (bytes.toString()) {
  //       return JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
  //     }
  //     return data;
  //   } catch (e) {
  //     console.log(e);
  //   }
  // }
}
