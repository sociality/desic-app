import { SessionsListComponent } from './sessions-list/sessions-list.component';
import { CreateSessionComponent } from './create-session/create-session.component';
import { PreviewSessionComponent } from './preview-session/preview-session.component';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

export const routes: Routes = [
  {
    path: 'list',
    component: SessionsListComponent,
  },
  {
    path: 'create',
    component: CreateSessionComponent,
  },
  {
    path: 'edit/:id',
    component: CreateSessionComponent,
  },
  {
    path: 'preview/:id',
    component: PreviewSessionComponent,
  },
  { path: '**', redirectTo: 'list' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SessionsRoutingModule {}
