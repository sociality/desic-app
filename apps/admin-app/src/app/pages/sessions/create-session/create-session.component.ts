import {
  Component,
  OnInit,
  OnDestroy,
  ChangeDetectorRef,
  ViewChild,
} from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormArray,
  Validators,
  AbstractControl,
} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { finalize, switchMap, takeUntil, tap } from 'rxjs/operators';

import { NbDialogService } from '@nebular/theme';
import { TranslateService } from '@ngx-translate/core';

import {
  APISessionsService,
  APICategoriesService,
  APIUsersService,
} from '@desic/api';
import {
  SessionInterface,
  CategoryInterface,
  UserInterface,
} from '@desic/models';
import { AppStateService } from '../../../services/state/state.service';

@Component({
  selector: 'create-session',
  templateUrl: './create-session.component.html',
  styleUrls: ['./create-session.component.scss'],
})
export class CreateSessionComponent implements OnInit, OnDestroy {
  @ViewChild('confirm_dialog') confirm_dialog;
  @ViewChild('response_dialog') response_dialog;
  @ViewChild('delete_dialog') delete_dialog;

  sessionID: string = null;
  itemExists: boolean = false;

  categories$: Observable<CategoryInterface[]>;
  users$: Observable<UserInterface[]>;

  sessionForm: FormGroup;
  patching = false;
  sending = false;
  private unsubscribe: Subject<any>;

  constructor(
    private fb: FormBuilder,
    private dialogService: NbDialogService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private changeDetectorRef: ChangeDetectorRef,
    private translate: TranslateService,
    private readonly sessionsService: APISessionsService,
    private readonly categoriesService: APICategoriesService,
    private readonly usersService: APIUsersService,
    private readonly appStateService: AppStateService
  ) {
    this.unsubscribe = new Subject();
  }

  ngOnInit(): void {
    setTimeout(() => {
      this.appStateService.setPageTitle('Sessions');
    });
    this.loadCategories();
    this.loadUsers();
    this.initForm();
    this.fetchParameter();
  }

  ngOnDestroy(): void {
    this.unsubscribe.next();
    this.unsubscribe.complete();
    this.sending = false;
    this.patching = false;
  }

  private loadIncident(id: string) {
    return this.sessionsService.getSessionById(id);
  }

  private loadCategories() {
    this.categories$ = this.categoriesService.getCategories('session');
  }

  private loadUsers() {
    this.users$ = this.usersService.getUsersByAccess('10');
  }

  private fetchParameter() {
    this.activatedRoute.params
      .pipe(
        switchMap((params) => {
          const id = params['id'];
          if (id) {
            this.sessionID = id;

            return this.loadIncident(id).pipe(
              tap((data) => {
                this.sessionForm.patchValue(this.formatDataOnFetch(data));
                this.sessionForm.controls['beneficiaryId'].disable();
                this.patching = true;
                this.changeDetectorRef.detectChanges();
              })
            );
          } else {
            this.patching = true;
            return Promise.resolve(true);
          }
        }),
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.changeDetectorRef.detectChanges();
        })
      )
      .subscribe();
  }

  private formatDataOnFetch(data: SessionInterface) {
    return data;
  }

  private formatDataOnPost(controls: { [key: string]: AbstractControl }) {
    return {
      title: controls.title.value,
      description: controls.description.value,

      sessionDate: controls.sessionDate.value,

      category: controls.category.value,
      keywords: controls.keywords.value,

      beneficiaryId: controls.beneficiaryId.value
        ? controls.beneficiaryId.value
        : '0',
    };
  }

  initForm() {
    this.sessionForm = this.fb.group({
      beneficiaryId: [, Validators.compose([Validators.required])],

      title: ['', Validators.compose([Validators.required])],
      description: ['', Validators.compose([Validators.required])],

      sessionDate: ['', Validators.compose([Validators.required])],
      category: ['', Validators.compose([Validators.required])],
      keywords: ['', Validators.compose([Validators.required])],
    });
  }

  public onFormSubmit() {
    const controls = this.sessionForm.controls;

    if (this.sessionForm.invalid) {
      Object.keys(controls).forEach((controlName) =>
        controls[controlName].markAsTouched()
      );
      return;
    }

    this.sending = true;

    const sessionData: SessionInterface = this.formatDataOnPost(controls);
    this.sessionID
      ? this.updateSession(this.sessionID, sessionData)
      : this.createSession(sessionData);
  }

  createSession(sessionData: SessionInterface) {
    this.responseHandler(this.sessionsService.postSession(sessionData));
  }

  updateSession(sessionID: string, sessionData: SessionInterface) {
    this.responseHandler(
      this.sessionsService.updateSession(sessionID, sessionData)
    );
  }

  responseHandler(message$: Observable<string>) {
    message$
      .pipe(
        tap(
          (data) => {
            this.openResponseDialog(this.response_dialog, {
              status: true,
              title: 'OK',
              message: data,
            });
          },
          (error) => {
            this.openResponseDialog(this.response_dialog, {
              status: false,
              title: 'Error',
              message: error.message,
            });
          }
        ),
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.sending = false;
          this.changeDetectorRef.markForCheck();
        })
      )
      .subscribe();
  }

  protected openResponseDialog(_dialog, _data) {
    const dialogRef = this.dialogService.open(_dialog, { context: _data });

    dialogRef.onClose.subscribe((result) => {
      if (_data.status) this.router.navigateByUrl('/sessions');
    });
  }

  public onClickBack(sessionId: string) {
    this.router.navigateByUrl(`/sessions/preview/${sessionId}`);
  }

  isControlHasError(controlName: string, validationType: string): boolean {
    const control = this.sessionForm.controls[controlName];
    if (!control) {
      return false;
    }

    const result =
      control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }
}

// createSession(sessionData: SessionInterface) {
//   const message$ = this.sessionsService.postSession(sessionData);
//   message$.subscribe(
//     (data) => {
//       this.openResponseDialog(this.response_dialog, {
//         status: true,
//         title: 'OK',
//         message: data,
//       });
//     },
//     (error) => {
//       this.openResponseDialog(this.response_dialog, {
//         status: false,
//         title: 'Error',
//         message: error.message,
//       });
//     }
//   );
// }

// updateSession(sessionID: string, sessionData: SessionInterface) {
//   const message$ = this.sessionsService.updateSession(sessionID, sessionData);
//   message$.subscribe(
//     (data) => {
//       this.openResponseDialog(this.response_dialog, {
//         status: true,
//         title: 'OK',
//         message: data,
//       });
//     },
//     (error) => {
//       this.openResponseDialog(this.response_dialog, {
//         status: false,
//         title: 'Error',
//         message: error.message,
//       });
//     }
//   );
// }
