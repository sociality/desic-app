import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbDatepickerModule,
  NbDialogModule,
  NbIconModule,
  NbInputModule,
  NbLayoutModule,
  NbListModule,
  NbRadioModule,
  NbSelectModule,
  NbTabsetModule,
  NbTooltipModule,
  NbUserModule,
} from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { TranslateModule } from '@ngx-translate/core';

import { SessionsRoutingModule } from './sessions-routing.module';
import { SessionsListComponent } from './sessions-list/sessions-list.component';
import { CreateSessionComponent } from './create-session/create-session.component';
import { PreviewSessionComponent } from './preview-session/preview-session.component';

import { PipesModule } from '../../pipes/pipes.module';
import { ViewsModule } from '../../views/views.module';

@NgModule({
  declarations: [
    SessionsListComponent,
    CreateSessionComponent,
    PreviewSessionComponent,
  ],
  imports: [
    CommonModule,

    NbButtonModule,
    NbCardModule,
    NbDatepickerModule.forRoot(),
    NbDialogModule.forRoot(),
    NbIconModule,
    NbInputModule,
    NbLayoutModule,
    NbRadioModule,
    NbSelectModule,
    NbTabsetModule,
    NbTooltipModule,
    NbEvaIconsModule,
    NbListModule,
    NbActionsModule,
    NbUserModule,

    FormsModule,
    ReactiveFormsModule,
    TranslateModule,

    SessionsRoutingModule,

    PipesModule,
    ViewsModule,
  ],
})
export class SessionsModule {}
