import { CategoriesListComponent } from './categories-list/categories-list.component';
import { EditCategoriesComponent } from './edit-categories/edit-categories.component';
import { EditOrganizationComponent } from './edit-organization/edit-organization.component';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminSettingsRoutingModule } from './setting-routing.module';
import {
  NbCardModule,
  NbInputModule,
  NbLayoutModule,
  NbButtonModule,
  NbTabsetModule,
  NbSelectModule,
  NbRadioModule,
  NbDatepickerModule,
  NbDialogModule,
  NbFormFieldModule,
  NbIconModule,
  NbTooltipModule,
} from '@nebular/theme';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ViewsModule } from '../../views/views.module';
import { NbEvaIconsModule } from '@nebular/eva-icons';

@NgModule({
  declarations: [
    CategoriesListComponent,
    EditCategoriesComponent,
    EditOrganizationComponent,
  ],
  imports: [
    CommonModule,
    AdminSettingsRoutingModule,
    NbCardModule,
    NbInputModule,
    NbLayoutModule,
    NbButtonModule,
    NbTabsetModule,
    NbSelectModule,
    NbRadioModule,
    NbIconModule,
    NbEvaIconsModule,
    NbDialogModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    NbFormFieldModule,
    NbTooltipModule,

    ViewsModule,
  ],
})
export class AdminSettingsModule {}
