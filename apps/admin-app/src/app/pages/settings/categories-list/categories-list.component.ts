import { Observable } from 'rxjs';
import { APICategoriesService } from '@desic/api';
import { CategoryInterface } from '@desic/models';

import { Component, OnInit } from '@angular/core';
import { AppStateService } from '../../../services/state/state.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'admin-categories-list',
  templateUrl: './categories-list.component.html',
  styleUrls: ['./categories-list.component.scss'],
})
export class CategoriesListComponent implements OnInit {
  categories$: Observable<CategoryInterface[]>;
  constructor(
    private readonly service: APICategoriesService,
    private activatedRoute: ActivatedRoute,
    private readonly appStateService: AppStateService
  ) {}

  ngOnInit(): void {
    this.appStateService.setPageTitle('Categories');
    this.fetchParameter();
  }

  private fetchParameter() {
    this.activatedRoute.params.subscribe((params) => {
      const type = params['type'];
      if (type) {
        this.loadCategories(type);
      } else {
        // this.patching = true;
      }
    });
  }

  private loadCategories(type) {
    this.categories$ = this.service.getCategories(type);
  }
}
