import { CategoriesListComponent } from './categories-list/categories-list.component';
import { EditCategoriesComponent } from './edit-categories/edit-categories.component';
import { EditOrganizationComponent } from './edit-organization/edit-organization.component';

import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

export const routes: Routes = [
  {
    path: 'organization',
    component: EditOrganizationComponent,
  },
  {
    path: 'categories/list/:type',
    component: CategoriesListComponent,
  },
  {
    path: 'categories/edit/:type',
    component: EditCategoriesComponent,
  },
  { path: '**', redirectTo: 'list' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminSettingsRoutingModule {}
