import {
  Component,
  OnInit,
  OnDestroy,
  ChangeDetectorRef,
  ViewChild,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { finalize, switchMap, takeUntil, tap } from 'rxjs/operators';

import { NbDialogService } from '@nebular/theme';
import { TranslateService } from '@ngx-translate/core';

import { APIExaminationsService } from '@desic/api';
import { ExaminationInterface } from '@desic/models';
import { AppStateService } from '../../../services/state/state.service';

@Component({
  selector: 'preview-examination',
  templateUrl: './preview-examination.component.html',
  styleUrls: ['./preview-examination.component.scss'],
})
export class PreviewExaminationComponent implements OnInit, OnDestroy {
  @ViewChild('response_dialog') response_dialog;
  @ViewChild('delete_dialog') delete_dialog;

  public examination$: Observable<ExaminationInterface>;

  patching: boolean = false;
  sending: boolean = false;
  private unsubscribe: Subject<any>;

  constructor(
    private dialogService: NbDialogService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private changeDetectorRef: ChangeDetectorRef,
    private translate: TranslateService,
    private readonly examinationsService: APIExaminationsService,
    private readonly appStateService: AppStateService
  ) {
    this.unsubscribe = new Subject();
  }

  ngOnInit(): void {
    setTimeout(() => {
      this.appStateService.setPageTitle('Examinations');
    });
    this.fetchParameter();
  }

  ngOnDestroy(): void {
    this.unsubscribe.next();
    this.unsubscribe.complete();
    this.patching = false;
    this.sending = false;
  }

  private loadExamination(id: string) {
    return this.examinationsService.getExaminationById(id);
  }

  private fetchParameter() {
    this.activatedRoute.params
      .pipe(
        tap((params) => {
          const id = params['id'];
          if (id) {
            this.examination$ = this.loadExamination(id);
            this.patching = true;
          } else {
            this.patching = true;
            return;
          }
        }),
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.changeDetectorRef.markForCheck();
        })
      )
      .subscribe();
  }

  public onClickBack() {
    this.router.navigateByUrl(`/examinations/list`);
  }

  public onClickEdit(examinationID: string) {
    this.router.navigateByUrl(`/examinations/edit/${examinationID}`);
  }

  public onClickDelete(examinationID: string, title: string) {
    this.openDeleteDialog(examinationID, title);
  }

  public onClickExport(examinationID: string) {
    this.router.navigateByUrl(`/examinations/edit/${examinationID}`);
  }

  deleteExamination(examinationID: string) {
    this.responseHandler(
      this.examinationsService.deleteExamination(examinationID)
    );
  }

  responseHandler(message$: Observable<string>) {
    message$
      .pipe(
        tap(
          (data) => {
            this.openResponseDialog(this.response_dialog, {
              status: true,
              title: 'OK',
              message: data,
            });
          },
          (error) => {
            this.openResponseDialog(this.response_dialog, {
              status: false,
              title: 'Error',
              message: error.message,
            });
          }
        ),
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.sending = true;
          this.changeDetectorRef.markForCheck();
        })
      )
      .subscribe();
  }

  protected openDeleteDialog(examinationID: string, title: string) {
    const dialogRef = this.dialogService.open(this.delete_dialog, {
      context: { slug: title },
    });
    dialogRef.onClose.subscribe((result) => {
      if (result) this.deleteExamination(examinationID);
    });
  }

  protected openResponseDialog(_dialog, _data) {
    const dialogRef = this.dialogService.open(_dialog, { context: _data });

    dialogRef.onClose.subscribe((result) => {
      if (_data.status) this.router.navigateByUrl('/examinations');
    });
  }
}

// private fetchParameter() {
//   this.activatedRoute.params.subscribe((params) => {
//     const id = params['id'];
//     if (id) {
//       this.loadExamination(id).subscribe((data) => {
//         this.examination = data;
//         this.patching = true;
//         this.changeDetectorRef.markForCheck();
//       });
//     } else {
//       this.patching = true;
//     }
//   }),
//     finalize(() => {
//       this.changeDetectorRef.markForCheck();
//     });
// }
