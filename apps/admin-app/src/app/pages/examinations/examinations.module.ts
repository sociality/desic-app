import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbDatepickerModule,
  NbDialogModule,
  NbIconModule,
  NbInputModule,
  NbLayoutModule,
  NbListModule,
  NbRadioModule,
  NbSelectModule,
  NbTabsetModule,
  NbTooltipModule,
  NbUserModule,
} from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { TranslateModule } from '@ngx-translate/core';

import { ExaminationsRoutingModule } from './examinations-routing.module';

import { ExaminationsListComponent } from './examinations-list/examinations-list.component';
import { CreateExaminationComponent } from './create-examination/create-examination.component';
import { PreviewExaminationComponent } from './preview-examination/preview-examination.component';

import { PipesModule } from '../../pipes/pipes.module';
import { ViewsModule } from '../../views/views.module';

@NgModule({
  declarations: [
    ExaminationsListComponent,
    CreateExaminationComponent,
    PreviewExaminationComponent,
  ],
  imports: [
    CommonModule,

    NbButtonModule,
    NbCardModule,
    NbDatepickerModule.forRoot(),
    NbDialogModule.forRoot(),
    NbIconModule,
    NbInputModule,
    NbLayoutModule,
    NbRadioModule,
    NbSelectModule,
    NbTabsetModule,
    NbTooltipModule,
    NbEvaIconsModule,
    NbListModule,
    NbActionsModule,
    NbUserModule,

    FormsModule,
    ReactiveFormsModule,
    TranslateModule,

    ExaminationsRoutingModule,

    PipesModule,
    ViewsModule,
  ],
})
export class ExaminationsModule {}
