import { Observable } from 'rxjs';
import { AppStateService } from './services/state/state.service';
import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'client-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'client-app';
  pageTitle$: Observable<string>;

  constructor(
    public translate: TranslateService,
    private readonly appStateService: AppStateService
  ) {
    this.pageTitle$ = this.appStateService.pageTitle$;
    translate.addLangs(['en']);
    translate.setDefaultLang('en');
  }
}
