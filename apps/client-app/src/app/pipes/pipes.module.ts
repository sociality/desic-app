import { NgModule } from '@angular/core';

import { FilterPipe } from './filter/filter.pipe';
import { AccessPipe } from './access/access.pipe';
import { UserPipe } from './user/user.pipe';
import { ContactsPipe } from './contacts/contacts.pipe';

@NgModule({
  imports: [],
  declarations: [FilterPipe, AccessPipe, UserPipe, ContactsPipe],
  exports: [FilterPipe, AccessPipe, UserPipe, ContactsPipe],
})
export class PipesModule {}
