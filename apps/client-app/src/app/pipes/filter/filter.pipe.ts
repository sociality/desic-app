import { Pipe, PipeTransform } from '@angular/core';
import { PostInterface } from '@desic/models';

@Pipe({
  name: 'filter_pipe',
  pure: false,
})
export class FilterPipe implements PipeTransform {
  transform(posts: PostInterface[], type: string) {
    if (posts) {
      if (type) {
        return posts.filter((o) => {
          return o.type === type;
        });
      } else {
        return posts;
      }
    } else {
      return [];
    }
  }
}
