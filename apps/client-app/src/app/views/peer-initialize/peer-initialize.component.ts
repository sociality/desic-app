import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';

import Peer from 'peerjs';
import * as CryptoJS from 'crypto-js';

/** Local Service & Interface */
import { LocalConnectionService } from '../../pages/connections/_connection.service';
import { LocalConnectionInterface } from '../../pages/connections/_connection.interface';
import { Observable, Subject, Subscription } from 'rxjs';
import { APIAuthService, APIConnectionsService } from '@desic/api';
import {
  AuthUserInterface,
  ConnectionInterface,
  UserInterface,
} from '@desic/models';

@Component({
  selector: 'client-peer-initialize',
  templateUrl: './peer-initialize.component.html',
  styleUrls: ['./peer-initialize.component.scss'],
})
export class PeerInitializeComponent implements OnInit, OnDestroy {
  public mypeerid: string = '';
  peer: any;
  public sessions: LocalConnectionInterface['PeerSessions'] = [];

  desicConnections$: Observable<ConnectionInterface[]>;
  desicConnections: LocalConnectionInterface['DesicConnections'];
  desicConnection: LocalConnectionInterface['DesicConnection'];

  private unsubscribe: Subject<any>;
  private subscription: Subscription = new Subscription();

  constructor(
    private changeDetectorRef: ChangeDetectorRef,
    private readonly connectionService: APIConnectionsService,
    private readonly authService: APIAuthService,
    private localConnectionService: LocalConnectionService
  ) {
    this.mypeerid = (this.authService.currentUser as AuthUserInterface)._id;
    this.subscription.add(
      this.localConnectionService.peer.subscribe((data) => (this.peer = data))
    );
    this.subscription.add(
      this.localConnectionService.peerSessions.subscribe(
        (data) => (this.sessions = data)
      )
    );
    this.subscription.add(
      this.localConnectionService.desicConnections.subscribe((data) => {
        this.desicConnections = data;
        console.log(data);
      })
    );
    this.subscription.add(
      this.localConnectionService.desicConnection.subscribe(
        (data) => (this.desicConnection = data)
      )
    );
    this.subscription.add(
      this.localConnectionService.anything.subscribe(
        (data) => (this.desicConnections$ = data)
      )
    );
  }

  ngOnInit(): void {
    this.initializePeer();
    this.loadConnections();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  private loadConnections() {
    this.desicConnections$ = this.connectionService.getConnectionsClient();
    this.localConnectionService.changeAnything(this.desicConnections$);
    this.desicConnections$.subscribe((data) => {
      this.desicConnections = data;
      console.log(data);
      this.localConnectionService.changeDesicConnections(this.desicConnections);
    });
  }

  private initializePeer() {
    this.peer = new Peer(this.mypeerid);
    this.localConnectionService.changePeer(this.peer);

    this.peer.on('connection', (conn: any) => {
      conn.on('data', (data: any) => {
        this.storeMessages(conn, data);
        this.changeDetectorRef.detectChanges();
      });

      conn.on('error', (error: any) => {
        console.log(`Error (Incoming Data): ${error}`);
      });
    });

    this.peer.on('error', (error: any) => {
      console.log(`Error (on Peer Connection): ${error}`);
    });
  }

  storeMessages(conn, message) {
    const secret: string = this.desicConnections.filter((o) => {
      return o.receiver.id == conn.peer;
    })[0].secret;
    const connectionId: string = this.desicConnections.filter((o) => {
      return o.receiver.id == conn.peer;
    })[0].id;
    const receiver: UserInterface = this.desicConnections.filter((o) => {
      return o.receiver.id == conn.peer;
    })[0].receiver;

    if (this.sessions && !this.sessions[0].connectionId) {
      setTimeout(() => {
        this.sessions[0] = {
          peer: conn.peer,
          secret: secret,
          connectionId: connectionId,
          messages: [
            this.messageFormat(this.decryptData(message, secret), receiver),
            //{ sender: 'other', text: this.decryptData(message, secret) },
          ],
          unread:
            connectionId == this.desicConnection.id
              ? 0
              : this.sessions[0].unread + 1,
        };
        this.changeDetectorRef.markForCheck();
      });
    } else {
      const index = this.sessions
        .map((o) => {
          return o.peer;
        })
        .indexOf(conn.peer);
      console.log('I get an index: ', index);

      if (index < 0) {
        setTimeout(() => {
          this.sessions.push({
            peer: conn.peer,
            secret: secret,
            connectionId: connectionId,
            messages: [
              this.messageFormat(this.decryptData(message, secret), receiver),
              // { sender: 'other', text: this.decryptData(message, secret) },
            ],
            unread:
              connectionId == this.desicConnection.id
                ? 0
                : this.sessions[index].unread,
          });
          this.changeDetectorRef.markForCheck();
        });
      } else {
        setTimeout(() => {
          this.sessions[index].messages.push(
            this.messageFormat(this.decryptData(message, secret), receiver)
          );
          this.changeDetectorRef.markForCheck();
        });
        //   {
        //   sender: 'other',
        //   text: this.decryptData(message, secret),
        // }
        if (connectionId == this.desicConnection.id) {
          this.sessions[index].unread = 0;
        } else {
          this.sessions[index].unread = this.sessions[index].unread + 1;
        }
      }
    }
    this.localConnectionService.changePeerSessions(this.sessions);

    console.log('My messages');
    console.log(this.sessions);
  }

  public messageFormat(message: string, receiver: UserInterface) {
    // const files = !event.files ? [] : event.files.map((file) => {
    //   return {
    //     url: file.src,
    //     type: file.type,
    //     icon: 'file-text-outline',
    //   };
    // });

    return {
      text: message,
      date: new Date(),
      reply: false,
      type: 'text', //files.length ? 'file' : 'text',
      files: [], //files,
      user: {
        name: receiver.name,
        //avatar: 'https://i.gifer.com/no.gif',
      },
    };
  }

  getTotalUnread() {
    return this.sessions.reduce((accum, item) => accum + item.unread, 0);
  }

  /** Encrypt & Decrypt Messages */
  encryptData(data: string, secret: string) {
    try {
      return CryptoJS.AES.encrypt(JSON.stringify(data), secret).toString();
    } catch (e) {
      console.log(e);
    }
  }

  decryptData(data: any, secret: string) {
    try {
      const bytes = CryptoJS.AES.decrypt(data, secret);
      if (bytes.toString()) {
        return JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
      }
      return data;
    } catch (e) {
      console.log(e);
    }
  }
}
