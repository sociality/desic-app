import { waitForAsync, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientPersonalMenuComponent } from './personal-menu.component';

describe('ClientPersonalMenuComponent', () => {
  let component: ClientPersonalMenuComponent;
  let fixture: ComponentFixture<ClientPersonalMenuComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [ClientPersonalMenuComponent],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientPersonalMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
