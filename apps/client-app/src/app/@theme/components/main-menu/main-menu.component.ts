import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { convertCompilerOptionsFromJson } from 'typescript';
import { TranslateService } from '@ngx-translate/core';
import { MENU_ITEMS } from '../../../menu-items.config';
import { NbMenuItem } from '@nebular/theme';

interface MenuItem extends NbMenuItem {
  slug: string;
}

@Component({
  selector: 'client-main-menu',
  templateUrl: './main-menu.component.html',
  styleUrls: ['./main-menu.component.scss'],
})
export class ClientMainMenuComponent implements OnInit {
  items: MenuItem[] = MENU_ITEMS;

  constructor(
    private router: Router,
    private readonly translate: TranslateService
  ) {}

  ngOnInit(): void {}

  // TODO: not possible to use the routerLinkActive
  // https://github.com/akveo/nebular/issues/2374
  isPageActive(path: string) {
    if (path === '/') {
      return path === this.router.url;
    }
    // As there ar redirect
    return this.router.url.startsWith(path);
  }
}
