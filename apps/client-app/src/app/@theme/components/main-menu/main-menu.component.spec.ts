import { waitForAsync, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientMainMenuComponent } from './main-menu.component';

describe('ClientMainMenuComponent', () => {
  let component: ClientMainMenuComponent;
  let fixture: ComponentFixture<ClientMainMenuComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [ClientMainMenuComponent],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientMainMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
