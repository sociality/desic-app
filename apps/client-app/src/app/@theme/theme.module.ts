import { RouterModule } from '@angular/router';
import { ClientHeaderComponent } from './components/header/header.component';
import {
  NbIconModule,
  NbActionsModule,
  NbPopoverModule,
  NbContextMenuModule,
  NbSelectModule,
  NbTooltipModule,
  NbButtonModule,
  NbThemeModule,
  NbLayoutModule,
  NbBadgeModule,
  NbSidebarModule,
  NbMenuModule,
} from '@nebular/theme';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClientMainMenuComponent } from './components/main-menu/main-menu.component';
import { ClientPersonalMenuComponent } from './components/personal-menu/personal-menu.component';
import { TranslateModule } from '@ngx-translate/core';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { ViewsModule } from '../views/views.module';
import { LocalConnectionService } from '../pages/connections/_connection.service';
import { PipesModule } from '../pipes/pipes.module';

import { ClientLayoutComponent } from './layouts/layout/layout.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,

    NbThemeModule.forRoot({ name: 'default' }),
    NbIconModule,
    NbLayoutModule,
    NbEvaIconsModule,
    NbSidebarModule.forRoot(),
    NbMenuModule.forRoot(),
    NbBadgeModule,
    NbTooltipModule,
    NbBadgeModule,
    NbButtonModule,
    NbActionsModule,
    NbPopoverModule,
    NbContextMenuModule,
    NbSelectModule,

    TranslateModule,
    ViewsModule,
    PipesModule,
  ],
  declarations: [
    ClientHeaderComponent,
    ClientMainMenuComponent,
    ClientPersonalMenuComponent,
    ClientLayoutComponent,
  ],
  exports: [
    ClientHeaderComponent,
    ClientMainMenuComponent,
    ClientPersonalMenuComponent,
  ],
  providers: [LocalConnectionService],
})
export class ThemeModule {}
