import { LocationStrategy } from '@angular/common';
import {
  Component,
  HostListener,
  Inject,
  Input,
  OnInit,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { Router } from '@angular/router';
import { PostInterface, POST_TYPES, NEBULAR_STATUS } from '@desic/models';
import { NbDialogRef } from '@nebular/theme';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';

@Component({
  selector: 'client-app-post-single',
  templateUrl: './post-single.component.html',
  styleUrls: ['./post-single.component.scss'],
})
export class AppPostSingleComponent implements OnInit {
  private prevOnSameUrlNavigation;
  public Editor = ClassicEditor;

  post: PostInterface;
  postTypes = POST_TYPES;
  backgroundImage: string = '';

  constructor(
    public dialogRef: NbDialogRef<AppPostSingleComponent>,
    private locationStrategy: LocationStrategy,
    private router: Router
  ) {}

  isEventPost(post: PostInterface): boolean {
    return post.type === POST_TYPES.EVENT;
  }

  getPostType(post: PostInterface) {
    let postType = '';
    switch (post.type) {
      case POST_TYPES.EVENT:
        postType = 'Events';
        break;
      case POST_TYPES.EDUCATIONAL:
        postType = 'Educationals';
        break;
      case POST_TYPES.POST:
        postType = 'Articles';
        break;
    }
    return postType;
  }

  getCardStatus(post: PostInterface): NEBULAR_STATUS | string {
    let status: NEBULAR_STATUS | string = '';

    switch (post.type) {
      case POST_TYPES.EVENT:
        status = NEBULAR_STATUS.INFO;
        break;
      case POST_TYPES.EDUCATIONAL:
        status = NEBULAR_STATUS.WARNING;
        break;
      case POST_TYPES.POST:
        status = NEBULAR_STATUS.SUCCESS;
        break;
      default:
        status = '';
    }
    return status;
  }

  closeDialog() {
    this.dialogRef.close();
  }

  ngOnInit(): void {
    this.prevOnSameUrlNavigation = this.router.onSameUrlNavigation;
    this.router.onSameUrlNavigation = 'ignore';
    this.locationStrategy.pushState(
      'Modal state',
      'Modal title',
      this.router.url,
      ''
    );
    this.backgroundImage = this.post.image_url
      ? `url(${this.post.image_url})`
      : "url('https://picsum.photos/1200/300?random=1)";
  }

  @HostListener('window:popstate', ['$event'])
  public onPopState(event: PopStateEvent) {
    this.dialogRef.close();
    event.preventDefault();
    event.stopPropagation();
    event.cancelBubble = true;
  }

  ngOnDestroy(): void {
    this.router.onSameUrlNavigation = this.prevOnSameUrlNavigation;
  }

  replaceAccents(value: string) {
    return value
      .replace(/Ά|Α|ά/g, 'α')
      .replace(/Έ|Ε|έ/g, 'ε')
      .replace(/Ή|Η|ή/g, 'η')
      .replace(/Ί|Ϊ|Ι|ί|ΐ|ϊ/g, 'ι')
      .replace(/Ό|Ο|ό/g, 'ο')
      .replace(/Ύ|Ϋ|Υ|ύ|ΰ|ϋ/g, 'υ')
      .replace(/Ώ|Ω|ώ/g, 'ω')
      .replace(/Σ|ς/g, 'σ');
  }
}
