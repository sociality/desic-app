import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AppPostsRoutingModule } from './posts-routing.module';
import { AppPostsListComponent } from './posts-list/posts-list.component';
import { AppPostCardComponent } from './post-card/post-card.component';
import { AppPostSingleComponent } from './post-single/post-single.component';
import {
  NbBadgeModule,
  NbCardModule,
  NbIconModule,
  NbTabsetModule,
  NbDialogModule,
  NbWindowModule,
} from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';

import { TranslateModule } from '@ngx-translate/core';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { PipesModule } from '../../pipes/pipes.module';
import { ViewsModule } from '../../views/views.module';

@NgModule({
  declarations: [
    AppPostsListComponent,
    AppPostCardComponent,
    AppPostSingleComponent,
  ],
  entryComponents: [AppPostSingleComponent],
  imports: [
    CommonModule,
    AppPostsRoutingModule,
    NbCardModule,
    NbBadgeModule,
    NbIconModule,
    NbTabsetModule,
    NbEvaIconsModule,
    NbDialogModule.forRoot(),
    NbWindowModule.forRoot(),
    TranslateModule,
    CKEditorModule,

    PipesModule,
    ViewsModule,
  ],
})
export class AppPostsModule {}
