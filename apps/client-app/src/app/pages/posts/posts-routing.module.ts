import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppPostsListComponent } from './posts-list/posts-list.component';

const routes: Routes = [
  {
    path: 'list',
    component: AppPostsListComponent,
  },
  { path: '**', redirectTo: 'list' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AppPostsRoutingModule {}
