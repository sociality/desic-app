import { ConnectionInterface } from '@desic/models';

interface DesicConnection {
  id: string;
  secret: string;
  sender: {
    id: string;
    email: string;
    name: string;
    access: number;
  };
  senderId: string;
  receiver: {
    id: string;
    email: string;
    name: string;
    access: number;
  };
  receiverId: string;
}

interface PeerConnection {}

/**
 *     text: message,
    date: new Date(),
    reply: true,
    type: 'text', //files.length ? 'file' : 'text',
    files: [], //files,
    user: {
      name: this.desicConnection.sender.name
      //avatar: 'https://i.gifer.com/no.gif',
    },
 */
// interface PeerMessage {
//   sender: string; // ['me', 'other']
//   text: string;
// }

interface PeerMessage {
  text: string;
  date: Date;
  reply: boolean;
  type: string;
  files: any[];
  user: {
    name: string;
  };
}

interface PeerSession {
  peer: string;
  connectionId: string;
  secret: string;
  // conn: any;
  messages: PeerMessage[];
  unread: number;
}

export interface LocalConnectionInterface {
  DesicConnection: DesicConnection;
  DesicConnections: DesicConnection[];

  PeerConnection: PeerConnection;
  PeerMessage: PeerMessage;
  PeerSession: PeerSession;
  PeerSessions: PeerSession[];
}
