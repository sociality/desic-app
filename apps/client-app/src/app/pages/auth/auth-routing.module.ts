import { NbLoginComponent, NbLogoutComponent } from '@nebular/auth';

import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { AuthLoginComponent } from './auth-login/auth-login.component';
import { AuthRequestPasswordComponent } from './auth-request-password/auth-request-password.component';
import { AuthResetPasswordComponent } from './auth-reset-password/auth-reset-password.component';

export const routes: Routes = [
  // {
  //   // path: '',
  //   // component: AuthModalComponent,
  //   // children: [
  //   //   {
  //   path: '',
  //   component: AuthLoginComponent, // NbLoginComponent,
  //   // outlet: 'modal'
  // },
  // {
  //     path: 'register',
  //     component: NbRegisterComponent,
  // },
  {
    path: 'login',
    component: AuthLoginComponent, // NbLoginComponent,
    // outlet: 'modal'
  },
  {
    path: 'logout',
    component: NbLogoutComponent,
    // outlet: 'modal'
  },
  {
    path: 'request-password',
    component: AuthRequestPasswordComponent, // NbRequestPasswordComponent,
    // outlet: 'modal'
  },
  {
    path: 'reset-password',
    component: AuthResetPasswordComponent, // NbResetPasswordComponent,
    // outlet: 'modal'
  },
  { path: '**', redirectTo: 'login' },
  // ]
  // }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ClientAuthRoutingModule {}
