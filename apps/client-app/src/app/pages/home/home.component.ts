import { Component, OnInit } from '@angular/core';
import { APIOrganizationService } from '@desic/api';
import { InternalContactList, OrganizationInterface } from '@desic/models';
import { Observable } from 'rxjs';
import { AppStateService } from '../../services/state/state.service';
import { AppChoicesService } from '../../services/choices/choices.service';

@Component({
  selector: 'client-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class ClientHomeComponent implements OnInit {
  organization$: Observable<OrganizationInterface>;

  public contactsList: InternalContactList[] = [];

  constructor(
    private readonly organizationService: APIOrganizationService,
    private readonly appStateService: AppStateService,
    private readonly choicesService: AppChoicesService
  ) {
    this.contactsList = this.choicesService.getContactsList;
  }

  ngOnInit(): void {
    setTimeout(() => {
      this.appStateService.setPageTitle('Home');
    });
    this.loadOrganization();
  }

  private loadOrganization() {
    this.organization$ = this.organizationService.getOrganization();
  }
}
