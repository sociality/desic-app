import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AppDocumentsRoutingModule } from './documents-routing.module';
import { DocumentsListComponent } from './documents-list/documents-list.component';
import { PreviewDocumentComponent } from './preview-document/preview-document.component';
import {
  NbBadgeModule,
  NbCardModule,
  NbIconModule,
  NbTabsetModule,
  NbDialogModule,
  NbWindowModule,
  NbListModule,
  NbActionsModule,
  NbTooltipModule,
  NbUserModule,
  NbButtonModule,
} from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';

import { TranslateModule } from '@ngx-translate/core';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { PipesModule } from '../../pipes/pipes.module';
import { ViewsModule } from '../../views/views.module';

@NgModule({
  declarations: [DocumentsListComponent, PreviewDocumentComponent],
  entryComponents: [AppDocumentsRoutingModule],
  imports: [
    CommonModule,
    AppDocumentsRoutingModule,
    NbCardModule,
    NbBadgeModule,
    NbIconModule,
    NbTabsetModule,
    NbEvaIconsModule,
    NbListModule,
    NbActionsModule,
    NbTooltipModule,
    NbUserModule,
    NbButtonModule,
    NbDialogModule.forRoot(),
    NbWindowModule.forRoot(),
    TranslateModule,
    CKEditorModule,

    PipesModule,
    ViewsModule,
  ],
})
export class AppDocumentsModule {}
