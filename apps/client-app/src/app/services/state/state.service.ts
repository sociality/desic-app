import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { distinctUntilChanged } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class AppStateService {
  private _pageTitle$: BehaviorSubject<string> = new BehaviorSubject<string>(
    ''
  );

  pageTitle$: Observable<string> = this._pageTitle$
    .asObservable()
    .pipe(distinctUntilChanged());

  constructor() {}

  setPageTitle(title: string): void {
    this._pageTitle$.next(title);
  }
}
